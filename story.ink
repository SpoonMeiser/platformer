VAR screen = "hello"
VAR interactive = false

VAR health = 0

LIST entities = Alec, BarbicanLever

VAR drawbridge_state = 0

VAR current_room = -> throne_room
~ health = 6
-> main_loop

=== main_loop ===
~ interactive = true
~ entities = ()
<- current_room
<- entity_interactions
+ [death]
  -> death
-> DONE

=== death ===
Oh dear, you appear to have died!
I'll be honest with you, this isn't really implemented yet, so I'll give you some health back and we'll pretend it never happened...
~ health = 4
-> main_loop

=== go_to (-> new_room) ===
~ current_room = new_room
-> main_loop

=== entity_interactions ===
+ { entities ? Alec } [interact-alec] -> alec
+ { entities ? BarbicanLever } [interact-barbicanlever] -> barbicanlever

=== lower_barbican ===
~ screen = "lower_barbican"
+ [direction-left]
  -> go_to(-> drawbridge)
+ [direction-right]
  -> go_to(-> bailey)
+ [direction-up]
  -> go_to(-> upper_barbican)

=== upper_barbican ===
~ screen = "upper_barbican"
~ entities = BarbicanLever
+ [direction-left]
  -> go_to(-> above_drawbridge)
+ [direction-down]
  -> go_to(-> lower_barbican)

=== drawbridge ===
{
    - drawbridge_state == 0:
        ~ screen = "lowered_drawbridge"
    - else:
        ~ screen = "raised_drawbridge"
}
+ [direction-left]
  -> go_to(-> deadend2)
+ [direction-right]
  -> go_to(-> lower_barbican)
+ [direction-up]
  -> go_to(-> above_drawbridge)
+ [direction-down]
  -> go_to(-> moat)

=== moat ===
~ screen = "moat"
+ [direction-up]
  -> go_to(->drawbridge)

=== above_drawbridge ===
{
    - drawbridge_state == 0:
        ~ screen = "above_lowered_drawbridge"
    - else:
        ~ screen = "above_raised_drawbridge"
}
+ [direction-right]
  -> go_to(-> upper_barbican)
+ [direction-down]
  -> go_to(-> drawbridge)

=== deadend2 ===
~ screen = "deadend2"
+ [direction-right]
  -> go_to(-> drawbridge)

=== bailey ===
~ screen = "bailey"
+ [direction-left]
  -> go_to(-> lower_barbican)
+ [direction-right]
  -> go_to(-> keep_entrance)

=== keep_entrance ===
~ screen = "keep_entrance"
+ [direction-left]
  -> go_to(-> bailey)
+ [direction-right]
  -> go_to(-> throne_room)

=== throne_room ===
~ screen = "throne_room"
~ entities = Alec
+ [direction-left]
  -> go_to(-> keep_entrance)
+ [direction-right]
  -> go_to(-> deadend)

=== deadend ===
~ screen = "deadend"
+ [direction-left]
  -> go_to(-> throne_room)

=== alec ===
Hello, my name is Alec.
+ [Hello Alec.]
  It's a pleasure to meet you.
+ [Piss off Alec.]
  Well... That's uncalled for...
- Come talk to me any time.
-> main_loop

=== barbicanlever ===
~ drawbridge_state = 1 - drawbridge_state
There is a deafening clanking of chains and groaning of wood as machinery sets into action.
-> main_loop
