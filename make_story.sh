#!/bin/bash

set -x
set -e

INK_VER=1.0.0

PROJ_ROOT=$(cd "$(dirname $0)"; pwd)

INK_DIR=${PROJ_ROOT}/ink-${INK_VER}
INK_PATH=${INK_DIR}/inklecate.zip

if [[ ! -f ${INK_PATH} ]]; then
    mkdir -p "$INK_DIR"
    wget https://github.com/inkle/ink/releases/download/v${INK_VER}/inklecate_linux.zip -O ${INK_PATH}
    unzip ${INK_PATH} -d ${INK_DIR}
fi

${INK_DIR}/inklecate -o ${PROJ_ROOT}/src/story.json ${PROJ_ROOT}/story.ink
