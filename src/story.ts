import { Story as InkStory } from "inkjs";

interface Choice {
    text: string;
};

interface InkStoryInterface {
    currentChoices: Choice[];
    canContinue: boolean;
    variablesState: any;

    Continue(): string;
    ChooseChoiceIndex(index: number): void;
    ObserveVariable(name: string, fn: (varName: string, newValue: any) => void): void;
};

interface Story {
    currentChoices: string[];
    variablesState: any;

    next(): string | null;
    choose(choice: number | string): void;
    observeVariable(name: string, fn: (varName: string, newValue: any) => void): void;
};

class StoryWrapper implements Story {
    story: InkStoryInterface;

    constructor(storyData) {
        this.story = new InkStory(storyData);
    }

    get variablesState() {
        return this.story.variablesState;
    }

    get currentChoices() {
        return this.story.currentChoices.map(c => c.text);
    }

    next() {
        return this.story.canContinue ? this.story.Continue() : null;
    }

    getChoiceIndex(choiceText: string): number {
        return this.story.currentChoices.findIndex(c => c.text == choiceText);
    }

    choose(choice: number | string) {
        const choiceIndex = typeof choice === "number" ? choice : this.getChoiceIndex(choice);

        this.story.variablesState["interactive"] = false;
        return this.story.ChooseChoiceIndex(choiceIndex);
    }

    observeVariable(name, fn) {
        return this.story.ObserveVariable(name, fn);
    }
}

export default Story;
export {
    StoryWrapper,
};

