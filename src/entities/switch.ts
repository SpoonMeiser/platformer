import BoundingBox, { Vector } from "../boundingBox"
import Entity, { Interactable } from "../entity"

class Switch implements Entity, Interactable {
    interactName: string
    position: Vector = {x: 500, y: 460}
    dimension: Vector = {x: 40, y: 20}

    constructor(name) {
        this.interactName = name
    }

    getBoundingBox(): BoundingBox {
        return new BoundingBox(
            this.position,
            this.dimension
        );
    }

    update(): void {
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = "orange"
        ctx.fillRect(this.position.x, this.position.y, this.dimension.x, this.dimension.y)
    }

    getInteractHint(): string {
        return "Pull the lever"
    }
}

export default Switch
