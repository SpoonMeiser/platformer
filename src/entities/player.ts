import BoundingBox, { Vector } from "../boundingBox";
import Entity from "../entity";
import InputState from "../inputState";
import Tilemap from "../tilemap";
import debounce, { debounceFunction } from "../debounce";
import UpdateContext from "../updateContext";
import sprites, { Sprite } from "../sprites"

function *getFrame(totalFrames: number, pause: number = 1): Generator<number> {
    while(true) {
        for (let frame = 0; frame < totalFrames; frame++) {
            for (let update = 0; update < pause; update++) {
                yield frame + 1
            }
        }
    }
}

class Player implements Entity {
    position: Vector = {x: 475, y: 265};
    dimension: Vector = {x: 25, y: 55};
    velocity: Vector = {x: 0, y: 0};
    debounceJump: debounceFunction = debounce(true);
    invulnerableFrames: number = 0;
    direction: "left" | "right" = "right"
    state: "run" | "standing" = "standing"
    runFrame: Generator<number> = getFrame(6, 3)

    getBoundingBox(): BoundingBox {
        return new BoundingBox(
            this.position,
            this.dimension,
        );
    }

    update(tilemap: Tilemap, input_state: InputState): void {
        const debouncedJump = this.debounceJump(input_state.jump);

        if (input_state.left) {
            this.velocity.x-= 2;
            this.direction = "left"
        }
        if (input_state.right) {
            this.velocity.x+= 2;
            this.direction = "right"
        }

        this.state = input_state.left || input_state.right ? "run" : "standing"

        if (tilemap && tilemap.supports( this.getBoundingBox() )) {
            this.velocity.y = Math.min(debouncedJump ? -14 : 0, this.velocity.y);
        }
        else {
            this.velocity.y+= 1;
        }

        const max_speed = 8;
        const speed = Math.abs(this.velocity.x);
        const direction = speed == 0 ? 0 : this.velocity.x/ speed;

        if (speed > max_speed) {
            this.velocity.x= direction * max_speed;
        }

        // treat pressing both directions as if neither were pressed
        if (input_state.left == input_state.right) {
            const new_speed = Math.max(0, speed - 2);
            this.velocity.x= direction * new_speed;
        }

        const {new_position, new_velocity} = tilemap.resolve_movement(this.getBoundingBox(), this.velocity);
        this.position = new_position;
        this.velocity = new_velocity;

        if (this.invulnerableFrames > 0) {
            this.invulnerableFrames--;
        }
    }

    draw(ctx: CanvasRenderingContext2D): void {
        // flash if invulnerable
        if (this.invulnerableFrames % 2 == 1) return;

        /*
        ctx.fillStyle = "green";
        ctx.fillRect(this.position.x, this.position.y, this.dimension.x, this.dimension.y);
        */

        const frame = this.state == "run" ? this.runFrame.next().value : 1
        const sprite = sprites[`sarah-${this.state}-${this.direction}-${frame}`]
        ctx.drawImage(sprite.img, this.position.x + sprite.offset.x, this.position.y + sprite.offset.y)
    }

    damage(ctx: UpdateContext, damage: number) {
        if (!this.isInvulnerable()) {
            ctx.health -= damage;
            this.invulnerableFrames = 120;
        }
    }

    isInvulnerable(): boolean {
        return this.invulnerableFrames > 0;
    }
}

export default Player;
