import BoundingBox from "../boundingBox";
import InputState, { dummyInput } from "../inputState";
import Player from "./player";
import levels from "../test_levels.json";
import Tilemap from "../tilemap";
import UpdateContext from "../updateContext";

describe("Player", () => {
    const empty_tilemap = new Tilemap(levels.tilemaps.empty);
    const full_tilemap = new Tilemap(levels.tilemaps.full);
    const floor_tilemap = new Tilemap(levels.tilemaps.floor_at_10);

    function get_player() {
        const input_state: InputState = { ...dummyInput };
        const player = new Player();

        /* position player such that they are on the floor of the floor tilemap */
        player.position.y = 145;

        return {input_state, player};
    }

    it("should be stationary when neither left nor right is pressed", () => {
        const {input_state, player} = get_player();

        const original_x = player.position.x;
        player.update(empty_tilemap, input_state);
        expect(player.position.x).toEqual(original_x);
    });

    it("should move left when left key are pressed", () => {
        const {input_state, player} = get_player();

        player.position.x = 0;

        input_state.left = true;
        player.update(empty_tilemap, input_state);
        expect(player.position.x).toBeLessThan(0);
    });

    it("should move right when right key are pressed", () => {
        const {input_state, player} = get_player();

        player.position.x = 0;

        input_state.right = true;
        player.update(empty_tilemap, input_state);
        expect(player.position.x).toBeGreaterThan(0);
    });

    it("should accelerate as a direction is held", () => {
        const {input_state, player} = get_player();

        input_state.right = true;

        const xs = [];

        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);

        const accelerating = xs.every((x, index) => {
            if (index == 0) return true;

            const prev_index = index - 1;
            const next_index = index + 1;
            if (next_index >= xs.length) return true;

            const before_speed = x - xs[prev_index];
            const after_speed = xs[next_index] - x;
            return before_speed < after_speed;
        });

        expect(accelerating).toBe(true);
    });

    it("should not accellerate beyond maximum speed", () => {
        const {input_state, player} = get_player();

        input_state.right = true;

        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);

        const xs = [];

        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);

        const constant_speed = xs.every((x, index) => {
            if (index == 0) return true;

            const prev_index = index - 1;
            const next_index = index + 1;
            if (next_index >= xs.length) return true;

            const before_speed = x - xs[prev_index];
            const after_speed = xs[next_index] - x;
            return before_speed == after_speed;
        });

        expect(constant_speed).toBe(true);
    });

    it("should decelerate when keys are released", () => {
        const {input_state, player} = get_player();

        input_state.right = true;

        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);
        player.update(empty_tilemap, input_state);

        input_state.right = false;
        const xs = [];

        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);
        player.update(empty_tilemap, input_state);
        xs.push(player.position.x);

        const decelerating = xs.every((x, index) => {
            if (index == 0) return true;

            const prev_index = index - 1;
            const next_index = index + 1;
            if (next_index >= xs.length) return true;

            const before_speed = x - xs[prev_index];
            const after_speed = xs[next_index] - x;
            return before_speed > after_speed;
        });

        expect(decelerating).toBe(true);
    });

    it("should fall", () => {
        const {input_state, player} = get_player();

        input_state.right = true;

        const ys = [];

        ys.push(player.position.y);
        player.update(empty_tilemap, input_state);
        ys.push(player.position.y);
        player.update(empty_tilemap, input_state);
        ys.push(player.position.y);
        player.update(empty_tilemap, input_state);
        ys.push(player.position.y);

        const falling = ys.every((y, index) => {
            if (index == 0) return true;

            const prev_index = index - 1;
            const next_index = index + 1;
            if (next_index >= ys.length) return true;

            // the difference from the previous index is smaller than the difference to the next
            const before_speed = y - ys[prev_index];
            const after_speed = ys[next_index] - y;
            return before_speed < after_speed;
        });

        expect(falling).toBe(true);
    });

    it("should not fall if supported", () => {
        const {input_state, player} = get_player();

        const original_y = player.position.y;
        player.update(floor_tilemap, input_state);

        // somehow reference a tilemap that supports the player

        expect(player.position.y).toEqual(original_y);
    });

    it("should jump if supported and jump pressed", () => {
        const {input_state, player} = get_player();

        const original_y = player.position.y;
        input_state.jump = true;

        player.update(floor_tilemap, input_state);

        expect(player.position.y).toBeLessThan(original_y);
    });

    it("should not jump when jump pressed if not supported", () => {
        const {input_state, player} = get_player();

        const original_y = player.position.y;
        input_state.jump = true;

        player.update(empty_tilemap, input_state);

        expect(player.position.y).toBeGreaterThanOrEqual(original_y);
    });

    it("should only jump once if jump pressed and held", () => {
        const {input_state, player} = get_player();

        const original_y = player.position.y;
        input_state.jump = true;

        // wait until player returns lands from the first jump
        for (let i = 0; i < 1000; i++) {
            player.update(floor_tilemap, input_state);
            if (player.position.y == original_y) break;
        }

        // make sure we fail here if the player's jump hasn't landed after 1000
        // frames
        expect(player.position.y).toEqual(original_y);

        // ensure the player doesn't start another jump in the next few frames
        // with jump still held down
        for (let i = 0; i < 5; i++) {
            player.update(floor_tilemap, input_state);
            expect(player.position.y).toEqual(original_y);
        }
    });

    /*
    // ideally would want to test this after a jump as well
    it("should jump if jump pressed before hitting the floor", () => {
        const {input_state, player} = get_player();

        // move the player slightly off the floor
        player.position.y -= 1;

        input_state.jump = true;

        function diff_y() {
            const old_y = player.position.y;
            player.update(floor_tilemap, input_state);
            return player.position.y - old_y;
        }

        // fall to the floor in the first frame;
        expect(diff_y()).toBeGreaterThan(0);

        // jumping in the next frame, despite the jump input not being released
        expect(diff_y()).toBeLessThan(0);
    });
    */

    it("should not stop a jump if supported", () => {
        // if the player being supported should stop a player that is falling,
        // but not stop a player that is in the ascending part of a jump
        const {input_state, player} = get_player();

        const original_y = player.position.y;
        player.velocity.y = -10;
        player.update(floor_tilemap, input_state);

        expect(player.position.y).toBeLessThan(original_y);
    });

    describe("damage", () => {
        const fakeCtx: UpdateContext = {
            entities: [],
            player: null,
            screen: null,
            health: 1,
        };

        it("should reduce health accordingly when damaged", () => {
            const {player} = get_player();
            const ctx = {
                ...fakeCtx,
                player: player,
                health: 5,
            };

            player.damage(ctx, 2);

            expect(ctx.health).toBe(3);
        });

        it("should cause the player to become invulnerable", () => {
            const {player} = get_player();
            const ctx = {
                ...fakeCtx,
                player: player,
            };

            player.damage(ctx, 2);

            expect(player.isInvulnerable()).toBeTruthy();
        });

        it("should cause the player to become invulnerable, but wear off after a while", () => {
            const {input_state, player} = get_player();
            const ctx = {
                ...fakeCtx,
                player: player,
            };

            player.damage(ctx, 2);

            expect(player.isInvulnerable()).toBeTruthy();

            // sometime in the next 300 frames, the invulnerability should run out
            for (let i = 0; i < 300; i++) {
                player.update(floor_tilemap, input_state);
                if (!player.isInvulnerable()) break;
            }

            expect(player.isInvulnerable()).toBeFalsy();
        });

        it("should not cause damage when invulnerable", () => {
            const {player} = get_player();
            const ctx = {
                ...fakeCtx,
                player: player,
                health: 5,
            };

            player.damage(ctx, 2);
            // health should be 3

            player.damage(ctx, 2);

            expect(ctx.health).toBe(3);

        });

        it("should not extend existing invulnerability", () => {
            const {input_state, player} = get_player();
            const ctx = {
                ...fakeCtx,
                player: player,
            };

            player.damage(ctx, 2);

            expect(player.isInvulnerable()).toBeTruthy();

            // sometime in the next 300 frames, the invulnerability should run out
            for (let i = 0; i < 300; i++) {
                player.damage(ctx, 2);
                player.update(floor_tilemap, input_state);
                if (!player.isInvulnerable()) break;
            }

            expect(player.isInvulnerable()).toBeFalsy();
        });
    });
});
