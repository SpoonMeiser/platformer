import BoundingBox, { Vector } from "../boundingBox";
import Entity, { Interactable } from "../entity";

class Alec implements Entity, Interactable {
    interactName = "alec";
    position: Vector = {x: 45, y: 390};

    getBoundingBox(): BoundingBox {
        return new BoundingBox(
            this.position,
            {x: 50, y: 50}
        );
    }

    update(): void {
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = "red";
        ctx.fillRect(this.position.x, this.position.y, 50, 50);
    }

    getInteractHint(): string {
        return "Talk to Alec";
    }
}

export default Alec
