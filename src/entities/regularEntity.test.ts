import createEntity from "./regularEntity";
import Tilemap, { TILESIZE } from "../tilemap";
import levels from "../test_levels.json";
import { dummyInput } from "../inputState";

describe("Patroller", () => {
    const floorTilemap = new Tilemap(levels.tilemaps.floor_at_10);

    const patrollerData = {"position": [25, 10]}

    it("should start centred on its tile coordinate", () => {
        const patrollerX = 25;
        const patrollerY = 10;
        const patrollerData = {"position": [patrollerX, patrollerY]}

        const patroller = createEntity("Patroller", patrollerData);
        const patrollerBB = patroller.getBoundingBox();

        const xCentre = patrollerBB.position.x + (patrollerBB.dimension.x / 2);
        const bottom = patrollerBB.position.y + patrollerBB.dimension.y;

        expect(xCentre).toEqual((TILESIZE * patrollerX) + (TILESIZE / 2));
        expect(bottom).toEqual(TILESIZE * patrollerY);
    });

    it("should move", () => {
        const patroller = createEntity("Patroller", patrollerData);

        const getX = () => patroller.getBoundingBox().position.x;

        const oldX = getX();
        patroller.update(floorTilemap, dummyInput);
        const newX = getX();

        expect(newX).not.toEqual(oldX);
    });

    it("should change direction when it encounters a wall", () => {
        const patroller = createEntity("Patroller", patrollerData);

        const tilemap = new Tilemap(levels.tilemaps.floor_at_10);
        tilemap.addTile("X", 20, 9);
        tilemap.addTile("X", 30, 9);

        const withinBounds = () => {
            const patrollerBB = patroller.getBoundingBox();
            if (patrollerBB.position.x < 21 * TILESIZE) {
                return false;
            }
            if (patrollerBB.position.x + patrollerBB.dimension.x > 30 * TILESIZE) {
                return false;
            }
            return true;
        }

        const getX = () => patroller.getBoundingBox().position.x;

        // we're actually testing that for 200 frames, it moves in both
        // directions and stays between the two "walls" we've put down
        let oldX = getX();
        let movedLeft = false;
        let movedRight = false;
        for (let i = 0; i < 200; i++) {
            patroller.update(tilemap, dummyInput);

            const newX = getX();
            const speed = newX - oldX;
            // console.log(`x: ${newX} (speed: ${speed})`);
            if (speed > 0) {
                movedRight = true;
            }
            if (speed < 0) {
                movedLeft = true;
            }

            oldX = newX;

            expect(withinBounds()).toBe(true);
        }

        expect(movedLeft).toBe(true);
        expect(movedRight).toBe(true);
    });

    it("should change direction at the end of a platform", () => {
        const patroller = createEntity("Patroller", patrollerData);

        const tilemap = new Tilemap(levels.tilemaps.floor_at_10);

        tilemap.addTile(" ", 20, 10);
        tilemap.addTile(" ", 19, 10);
        tilemap.addTile(" ", 18, 10);
        tilemap.addTile(" ", 17, 10);

        tilemap.addTile(" ", 30, 10);
        tilemap.addTile(" ", 31, 10);
        tilemap.addTile(" ", 32, 10);
        tilemap.addTile(" ", 33, 10);

        const withinBounds = () => {
            const patrollerBB = patroller.getBoundingBox();
            if (patrollerBB.position.x < 21 * TILESIZE) {
                return false;
            }
            if (patrollerBB.position.x + patrollerBB.dimension.x > 30 * TILESIZE) {
                return false;
            }
            return true;
        }

        const getX = () => patroller.getBoundingBox().position.x;

        // we're actually testing that for 200 frames, it moves in both
        // directions and stays between the two "holes" we've put down
        let oldX = getX();
        let movedLeft = false;
        let movedRight = false;
        for (let i = 0; i < 200; i++) {
            patroller.update(tilemap, dummyInput);

            const newX = getX();
            const speed = newX - oldX;
            //console.log(`x: ${newX} (speed: ${speed})`);
            if (speed > 0) {
                movedRight = true;
            }
            if (speed < 0) {
                movedLeft = true;
            }

            oldX = newX;

            expect(withinBounds()).toBe(true);
        }

        expect(movedLeft).toBe(true);
        expect(movedRight).toBe(true);
    });
});
