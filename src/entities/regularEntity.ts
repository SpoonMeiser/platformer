import Entity, { Collidable } from "../entity";
import BoundingBox, { Vector } from "../boundingBox";
import { TILESIZE } from "../tilemap";
import Player from "./player";


class Patroller implements Entity, Collidable {
    position: Vector;
    dimension: Vector = {x: 40, y: 45};
    velocity: Vector = {x: 1, y: 0};

    constructor(data) {
        const x = (data.position[0] * TILESIZE + TILESIZE/2) - (this.dimension.x / 2);
        const y = (data.position[1] * TILESIZE) - this.dimension.y;
        this.position = {x, y};
    }

    update(tilemap, input_state) {
        const {new_position, new_velocity} = tilemap.resolve_movement(this.getBoundingBox(), this.velocity);
        this.position = new_position;

        if (new_velocity.x == 0) {
            this.velocity.x = 0 - this.velocity.x;
        }

        // if we wouldn't be supported one width in the direction we're
        // travalling now, then we're on the edge of a hole; turn around
        const testBBox = new BoundingBox(
            {
                x: this.position.x + (Math.sign(this.velocity.x) * this.dimension.x),
                y: this.position.y
            },
            this.dimension
        )
        if (!tilemap.supports(testBBox)) {
            this.velocity.x = 0 - this.velocity.x;
        }
    }

    draw(ctx) {
        ctx.fillStyle = "purple";
        ctx.fillRect(this.position.x, this.position.y, this.dimension.x, this.dimension.y);
    }

    getBoundingBox() {
        return new BoundingBox(
            this.position,
            this.dimension,
        );
    }

    collide(ctx, other) {
        if (other instanceof Player) {
            other.damage(ctx, 1);
        }
    }
}

type EntityType = string;

function createEntity(entityType: EntityType, data: any): Entity {
    if (entityType == "Patroller") {
        return new Patroller(data);
    }

    throw new Error(`Unknown entity type '${entityType}'`);
}


export default createEntity;
