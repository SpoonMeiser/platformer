interface InputState {
    left: boolean;
    right: boolean;
    up: boolean;
    down: boolean;
    jump: boolean;
    interact: boolean;
}

// for easily initialising test fakes
const dummyInput: InputState = {
    left: false,
    right: false,
    up: false,
    down: false,
    jump: false,
    interact: false,
}

class GamepadState implements InputState  {
    gamepad_index: number | null = null;

    constructor(window?: Window) {
        if (window) {
            window.addEventListener(
                "gamepadconnected",
                (e: GamepadEvent) => this.handleGamepadConnected(e)
            );
        }
    }

    handleGamepadConnected(event : GamepadEvent) {
        this.gamepad_index = event.gamepad.index;
    }

    getGamepad() {
        if (this.gamepad_index == null) {
            return null;
        }

        const gamepads = navigator.getGamepads();
        return gamepads[this.gamepad_index];
    }

    get left() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.axes[0] < -0.5 : false;
    }

    get right() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.axes[0] > 0.5 : false;
    }

    get up() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.axes[0] < -0.5 : false;
    }

    get down() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.axes[0] > 0.5 : false;
    }

    get jump() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.buttons[0].pressed : false;
    }

    get interact() {
        const gamepad = this.getGamepad();
        return gamepad ? gamepad.buttons[0].pressed : false;
    }
}

class KeyState implements InputState {
    left: boolean = false;
    right: boolean = false;
    up: boolean = false;
    down: boolean = false;
    jump: boolean = false;
    interact: boolean = false;

    constructor(window?: Window) {
        if (window) {
            window.addEventListener("keydown", e => this.handleKeyDown(e));
            window.addEventListener("keyup", e => this.handleKeyUp(e));
        }
    }

    handleKeyDown(event : KeyboardEvent) {
        const keycode = event.code;

        switch (keycode) {
            case "ArrowLeft":
                this.left = true;
                break;

            case "ArrowRight":
                this.right = true;
                break;

            case "ArrowUp":
                this.jump = true;
                this.up = true;
                break;

            case "ArrowDown":
                this.down = true;
                break;

            case "Space":
                this.interact = true;
                break;
        }
    }

    handleKeyUp(event : KeyboardEvent) {
        const keycode = event.code;

        switch (keycode) {
            case "ArrowLeft":
                this.left = false;
                break;

            case "ArrowRight":
                this.right = false;
                break;

            case "ArrowUp":
                this.jump = false;
                this.up = false;
                break;

            case "ArrowDown":
                this.down = false;
                break;

            case "Space":
                this.interact = false;
                break;
        }
    }
}

class CombinedInput implements InputState {
    key: InputState;
    gamepad: InputState;

    constructor(window?: Window) {
        this.key = new KeyState(window);
        this.gamepad = new GamepadState(window);
    }

    get left() {
        return this.key.left || this.gamepad.left;
    }

    get right() {
        return this.key.right || this.gamepad.right;
    }

    get up() {
        return this.key.up || this.gamepad.up;
    }

    get down() {
        return this.key.down || this.gamepad.down;
    }

    get jump() {
        return this.key.jump || this.gamepad.jump;
    }

    get interact() {
        return this.key.interact || this.gamepad.interact;
    }
}

export default InputState;
export {
    CombinedInput,
    dummyInput,
}
