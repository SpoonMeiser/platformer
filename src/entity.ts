import BoundingBox from "./boundingBox";
import InputState from "./inputState";
import Tilemap from "./tilemap";
import UpdateContext from "./updateContext";


interface Entity {
    update: (tilemap: Tilemap, input_state: InputState) => void,
    draw: (ctx: CanvasRenderingContext2D) => void,
    getBoundingBox: () => BoundingBox,
}

interface Interactable {
    interactName: string,
    getInteractHint: () => string,
}

interface Collidable {
    collide: (ctx: UpdateContext, other: Entity) => void,
}

function isAlsoInteractable(entity: Entity): entity is (Entity & Interactable) {
    return "getInteractHint" in entity;
}

function isAlsoCollidable(entity: Entity): entity is (Entity & Collidable) {
    return "collide" in entity;
}


export default Entity;
export {
    Interactable,
    isAlsoInteractable,
    Collidable,
    isAlsoCollidable,
}
