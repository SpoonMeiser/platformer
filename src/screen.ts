import Tilemap from "./tilemap";
import Entity from "./entity";
import createEntity from "./entities/regularEntity";


interface EntityData {
    type: string,
    data: any,
}

interface ScreenData {
    title: string,
    tilemap: string[],
    regular_entities?: EntityData[],
}

class Screen {
    title: string;
    tilemap: Tilemap;
    entities: Entity[];

    constructor(title, tilemap, entities=[]) {
        this.title = title;
        this.tilemap = tilemap;
        this.entities = entities;
    }

    static fromData(screenData: ScreenData) {
        const title = screenData.title;
        const tilemap = new Tilemap(screenData.tilemap);

        let entities: Entity[] = [];
        if (screenData.regular_entities) {
            entities = screenData.regular_entities.map(ed => createEntity(ed.type, ed.data));
        }

        return new Screen(title, tilemap, entities);
    }
}

export default Screen
export {
    ScreenData
};

