import InputState, { CombinedInput, dummyInput } from "./inputState";

describe("InputState", () => {
    it("should report left when either key or gamepad is left", () => {
        const input_state = new CombinedInput();
        const gamepad: InputState = { ...dummyInput };
        const key: InputState = { ...dummyInput };

        input_state.key = key;
        input_state.gamepad = gamepad;

        expect(input_state.left).toBe(false);

        key.left = true;
        gamepad.left = false;
        expect(input_state.left).toBe(true);

        key.left = false;
        gamepad.left = true;
        expect(input_state.left).toBe(true);

        key.left = true;
        gamepad.left = true;
        expect(input_state.left).toBe(true);

        key.left = false;
        gamepad.left = false;
        expect(input_state.left).toBe(false);
    });
});
