import BoundingBox, { Vector } from "./boundingBox";

const TILESIZE = 20;

type TileChar = ' ' | 'X' | '-';

function split_vector(vector: Vector): Vector[] {
    const biggest_component = Math.max(Math.abs(vector.x), Math.abs(vector.y));
    const sign = n => n == 0 ? 0 :n / Math.abs(n);

    const x_step_dist = biggest_component / Math.abs(vector.x);
    const y_step_dist = biggest_component / Math.abs(vector.y);

    const steps = [];
    for (let i = 0; i < biggest_component; i++) {
        if (vector.x != 0 && Math.floor(i % x_step_dist) == 0) steps.push({x: sign(vector.x), y: 0});
        if (vector.y != 0 && Math.floor(i % y_step_dist) == 0) steps.push({x: 0, y: sign(vector.y)});
    }

    return steps;
}

class Tilemap {
    data: string[];

    constructor(rows: string[]) {
        /*
        if (rows.length != 25) {
            throw new Error(`Wrong number of rows: ${rows.length} instead of 25`);
        }

        if (rows.some(r => r.length != 50)) {
            throw new Error("At least one row has the wrong number of columns");
        }
        */

        this.data = [...rows];
    }

    // to create tilemaps in testing
    addTile(tile: TileChar, x: number, y: number): void {
        const orig_row = this.data[y];
        const new_row = orig_row.substr(0, x) + tile + orig_row.substr(x + 1);

        this.data[y] = new_row;
    }

    debugDraw(ctx: CanvasRenderingContext2D): void {
        const colours = [
            ['#dff', '#ddf'],
            ['#fdf', '#fdd'],
        ];

        for (let x = 0; x < 50; x++) {
            for (let y = 0; y < 25; y++) {
                if (this.data[y][x] == 'X') {
                    ctx.fillStyle = 'black';
                }
                else if (this.data[y][x] == '-') {
                    ctx.fillStyle = 'blue';
                }
                else {
                    ctx.fillStyle = colours[x % 2][y % 2];
                }
                ctx.fillRect(x * TILESIZE, y * TILESIZE, TILESIZE, TILESIZE);
            }
        }
    }

    collides(box: BoundingBox, step?: Vector): boolean {
        const min_tile_x = Math.floor(box.position.x / TILESIZE);
        const min_tile_y = Math.floor(box.position.y / TILESIZE);

        const max_tile_x = Math.ceil((box.position.x + box.dimension.x) / TILESIZE);
        const max_tile_y = Math.ceil((box.position.y + box.dimension.y) / TILESIZE);

        for (let x = min_tile_x; x <= max_tile_x; x++) {
            for (let y = min_tile_y; y <= max_tile_y; y++) {
                if (x < 0 || x >= 50) continue;
                if (y < 0 || y >= 25) continue;
                if (this.data[y][x] == ' ') continue;

                if (this.data[y][x] == '-') {
                    // if we're not testing a downward movement, skip
                    const step_y = step?.y ?? 0;
                    if (step_y <= 0) continue;

                    // if before the step vector, we were not above the tile, skip
                    const box_bottom = box.position.y + box.dimension.y;
                    if (box_bottom - step_y > y*TILESIZE) continue;
                }

                const tile_box = new BoundingBox({x: x*TILESIZE, y: y*TILESIZE}, {x: TILESIZE, y: TILESIZE});
                if (box.collides(tile_box)) return true;
            }
        }

        return false;
    }

    supports(box: BoundingBox): boolean {
        // the box is supported if a 1px high box just under it collides with the tilemap
        const offset_box = new BoundingBox(
            {x: box.position.x, y: box.position.y + box.dimension.y},
            {x: box.dimension.x, y: 1}
        );

        return this.collides(offset_box, {x: 0, y: 1});
    }

    resolve_movement(bounding_box: BoundingBox, velocity: Vector) {
        let new_position = bounding_box.position;

        let new_velocity = {
            x: velocity.x,
            y: velocity.y,
        };

        const steps: Vector[] = split_vector(velocity);

        const isValidPosition = (position, step) => {
            const new_box = new BoundingBox(position, bounding_box.dimension);
            return !this.collides(new_box, step);
        }

        const isOffScreen = position => {
            const new_box = new BoundingBox(position, bounding_box.dimension);
            return this.isOffScreen(new_box);
        }

        for (let step of steps) {
            const temp_position = {x: new_position.x + step.x, y: new_position.y + step.y};

            if (isValidPosition(temp_position, step)) {
                new_position = temp_position;
            }
            else {
                new_velocity = {
                    x: step.x == 0 ? new_velocity.x : 0,
                    y: step.y == 0 ? new_velocity.y : 0,
                };
            }

            if (isOffScreen(new_position)) {
                break;
            }
        }

        return { new_position, new_velocity };
    }

    isOffScreen(box: BoundingBox): boolean {
        const {x, y} = box.getCenter();

        if (x < 0) return true;
        if (x > 1000) return true;
        if (y < 0) return true;
        if (y > 500) return true;

        return false;
    }
}

export default Tilemap;
export {
    split_vector,
    TILESIZE
};
