interface Vector {
    x: number,
    y: number
}

class BoundingBox {
    position: Vector;
    dimension: Vector;

    constructor(position: Vector, dimension: Vector) {
        this.position = position;
        this.dimension = dimension;
    }

    collides(other: BoundingBox): boolean {
        if (other.position.x >= (this.position.x + this.dimension.x)) return false;
        if (other.position.y >= (this.position.y + this.dimension.y)) return false;

        if ((other.position.x + other.dimension.x) <= this.position.x) return false;
        if ((other.position.y + other.dimension.y) <= this.position.y) return false;

        return true;
    }

    getCenter(): Vector {
        return {
            x: this.position.x + (this.dimension.x / 2),
            y: this.position.y + (this.dimension.y / 2)
        };
    }

    embiggen(margin: number): BoundingBox {
        return new BoundingBox(
            {
                x: this.position.x - margin,
                y: this.position.y - margin
            },
            {
                x: this.dimension.x + (margin * 2),
                y: this.dimension.y + (margin * 2)
            }
        );
    }

    isNear(other: BoundingBox): boolean {
        const interactableArea = this.embiggen(20);
        return interactableArea.collides(other);
    }
}

export default BoundingBox;
export {
    Vector,
};
