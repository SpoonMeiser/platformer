import Screen, { ScreenData } from "./screen";

describe("Screen", () => {
    it("should be initialised with ScreenData", () => {
        const data: ScreenData = {
            title: "title",
            tilemap: []
        };
        const screen = Screen.fromData(data);
    });

    it("should expose the title", () => {
        const title = "This is the title!";
        const data: ScreenData = {
            title: title,
            tilemap: []
        };
        const screen = Screen.fromData(data);
        expect(screen.title).toBe(title);
    });

    it("should expose the title", () => {
        const title = "This is the title!";
        const data: ScreenData = {
            title: title,
            tilemap: []
        };
        const screen = Screen.fromData(data);
        expect(screen.tilemap).toBeDefined();
    });
});
