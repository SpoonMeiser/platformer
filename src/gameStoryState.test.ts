import BoundingBox from "./boundingBox";
import GameStoryState, { Choices, Content, Interactive } from "./gameStoryState";
import InputState, { dummyInput } from "./inputState";
import Story from './story';
import Tilemap from "./tilemap";
import Screen from "./screen";
import UI from "./ui";
import Entity, { Interactable, Collidable } from "./entity";
import UpdateContext from "./updateContext";

describe("GameStoryState", () => {
    const fakeStory: Story = {
        variablesState: {},
        currentChoices: [],
        next: () => 'content',
        choose: function (i) {},
        observeVariable: function (...args) {}
    }

    const fakeUI: UI = {
        setScreenTitle: title => {},
        setContent: (content?) => {},
        setChoices: (choices, idx) => {},
        setInteractHint: (content?) => {},
        setHealth: (health) => {},
    };

    const fakeEntity: Entity = {
        update: (tilemap, inputState) => {},
        draw: (ctx) => {},
        getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
    };

    const fakeInteractableEntity: Entity & Interactable = {
        ...fakeEntity,
        interactName: 'name',
        getInteractHint: () => 'hint',
    };

    const fakePlayer: Entity = {
        ...fakeEntity,
    };

    const fakeCtx: UpdateContext = {
        entities: [],
        player: fakePlayer,
        screen: new Screen(
            "title",
            new Tilemap([])
        ),
        health: 1,
    };

    describe("nextState", () => {
        it("should return a content state is the story can continue", () => {
            const story: Story = {
                ...fakeStory,
            };

            const state = GameStoryState.nextState(story);

            expect(state).toBeInstanceOf(Content);
        });

        it("should return a content state with story content", () => {
            const content = "fake content";
            const story: Story = {
                ...fakeStory,
                next: () => content,
            };

            const state = GameStoryState.nextState(story);

            expect((<Content>state).content).toBe(content);
        });

        it("should return an interactive state if interactive set and cannot continue", () => {
            const story: Story = {
                ...fakeStory,
                variablesState: {"interactive": 1},
                next: () => null,
            };

            const state = GameStoryState.nextState(story);

            expect(state).toBeInstanceOf(Interactive);
        });

        it("should return a choices state if interactive not set and cannot continue", () => {
            const story: Story = {
                ...fakeStory,
                variablesState: {"interactive": 0},
                next: () => null,
            };

            const state = GameStoryState.nextState(story);

            expect(state).toBeInstanceOf(Choices);
        });

        it("should return a choices state with the available choices", () => {
            const choices = ["one", "two", "three"];
            const story: Story = {
                ...fakeStory,
                variablesState: {"interactive": 0},
                next: () => null,
                currentChoices: choices,
            };

            const state = GameStoryState.nextState(story);

            expect((<Choices>state).choices).toEqual(choices);
        });
    });

    const noInput: InputState = { ... dummyInput };
    const interactInput: InputState = { ...dummyInput, interact: true };
    const upInput: InputState = { ...dummyInput, up: true };
    const downInput: InputState = { ...dummyInput, down: true };

    describe("Content", () => {
        describe("update", () => {
            it("should return itself if interact not pushed", () => {
                const state = new Content(fakeStory);

                const new_state = state.update(fakeCtx, noInput);

                expect(new_state).toBe(state);
            });

            it("should return a new state if interact pushed", () => {
                let contents = ["bar", "foo"];
                const story: Story = {
                    ...fakeStory,
                    next: () => contents.pop(),
                };

                const state = new Content(story);

                const new_state = state
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, interactInput);

                expect(new_state).not.toBe(state);
                expect((<Content>state).content).toBe("foo");
                expect((<Content>new_state).content).toBe("bar");
            });

            it("should return itself if interact has not been released", () => {
                const story: Story = {
                    ...fakeStory,
                };

                const state = new Content(story);

                const new_state = state.update(fakeCtx, interactInput);

                expect(new_state).toBe(state);
            });

            it("should only update state when interact is released and pressed again", () => {
                let contents = ["baz", "bar", "foo"];
                const story: Story = {
                    ...fakeStory,
                    next: () => contents.pop(),
                };

                const s0 = new Content(story);

                const s1 = s0.update(fakeCtx, interactInput);
                const s2 = s1.update(fakeCtx, interactInput);
                const s3 = s2.update(fakeCtx, interactInput);
                const s4 = s3.update(fakeCtx, noInput);
                const s5 = s4.update(fakeCtx, noInput);
                const s6 = s5.update(fakeCtx, interactInput);
                const s7 = s6.update(fakeCtx, interactInput);
                const s8 = s7.update(fakeCtx, noInput);
                const s9 = s8.update(fakeCtx, interactInput);
                const s10 = s9.update(fakeCtx, interactInput);

                expect(s1).toBe(s0);
                expect(s2).toBe(s0);
                expect(s3).toBe(s0);
                expect(s4).toBe(s0);
                expect(s5).toBe(s0);

                expect(s6).not.toBe(s0);
                expect(s7).toBe(s6);
                expect(s8).toBe(s6);

                expect(s9).not.toBe(s8);
                expect(s10).toBe(s9);

                expect((<Content>s0).content).toBe("foo");
                expect((<Content>s6).content).toBe("bar");
                expect((<Content>s9).content).toBe("baz");
            });
        });

        describe("updateUI", () => {
            it("should set the UI content", () => {
                const content = "some text";
                const story: Story = {
                    ...fakeStory,
                    next: () => content,
                };
                const ui = {
                    setScreenTitle: title => {},
                    setContent: jest.fn((content?) => {}),
                    setChoices: (choices) => {},
                };

                const state = new Content(story);

                state.updateUI(fakeCtx, ui);

                expect(ui.setContent.mock.calls.length).toBe(1);
                expect(ui.setContent.mock.calls[0][0]).toBe(content);
            });
        });
    });

    describe("Choices", () => {
        describe("update", () => {
            it("should select option one if only interact is pressed", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "three"],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state = state
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, interactInput);

                expect(new_state).not.toBe(state);
                expect(chooseMock.mock.calls.length).toBe(1);
                expect(chooseMock.mock.calls[0][0]).toBe(0);
            });

            it("should not make a choice if interact not pushed", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "three"],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state = state.update(fakeCtx, noInput);

                expect(new_state).toBe(state);
                expect(chooseMock.mock.calls.length).toBe(0);
            });

            it("should not make a choice if interact initially held", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "three"],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state1 = state.update(fakeCtx, interactInput);
                const new_state2 = state.update(fakeCtx, interactInput);
                const new_state3 = state.update(fakeCtx, interactInput);

                expect(new_state1).toBe(state);
                expect(new_state2).toBe(state);
                expect(new_state3).toBe(state);
                expect(chooseMock.mock.calls.length).toBe(0);
            });

            it("should select option two if down pressed before interact", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "three"],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state = state
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, interactInput);

                expect(new_state).not.toBe(state);
                expect(chooseMock.mock.calls.length).toBe(1);
                expect(chooseMock.mock.calls[0][0]).toBe(1);
            });

            it("should cycle back to option one if down pressed when on last option", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "three"],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state = state
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, interactInput);

                expect(new_state).not.toBe(state);
                expect(chooseMock.mock.calls.length).toBe(1);
                expect(chooseMock.mock.calls[0][0]).toBe(0);
            });

            it("should only change the selected option once each time up or down pressed", () => {
                const chooseMock = jest.fn(i => {});
                const story: Story = {
                    ...fakeStory,
                    currentChoices: [
                        "one", "two", "three", "four", "five", "six", "seven"
                    ],
                    choose: chooseMock,
                };

                const state = new Choices(story);

                const new_state = state
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput) // down
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput) // down
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, upInput)   // up
                    .update(fakeCtx, upInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, downInput) // down
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, downInput)
                    .update(fakeCtx, noInput)
                    .update(fakeCtx, upInput)   // up
                    .update(fakeCtx, upInput)
                    .update(fakeCtx, upInput)
                    .update(fakeCtx, upInput)
                    .update(fakeCtx, interactInput);

                expect(new_state).not.toBe(state);
                expect(chooseMock.mock.calls.length).toBe(1);
                expect(chooseMock.mock.calls[0][0]).toBe(1);
            });
        });

        describe("updateUI", () => {
            it("should display choices", () => {
                const choice = 2
                const choices = ["one", "two", "three", "four", "five"];

                const story: Story = {
                    ...fakeStory,
                    currentChoices: choices,
                };

                const ui = {
                    ...fakeUI,
                    setChoices: jest.fn((choices, idx) => {}),
                };

                const state = new Choices(story, choice);

                state.updateUI(fakeCtx, ui);

                expect(ui.setChoices.mock.calls.length).toBe(1);
                expect(ui.setChoices.mock.calls[0])
                    .toEqual([choices, choice]);
            });
        });
    });

    describe("Interactive", () => {
        describe("update", () => {
            it("should call update on context entities", () => {
                const entity = {
                    ...fakeEntity,
                    update: jest.fn((tilemap, input) => {})
                };
                const input = { ...dummyInput };
                const ctx: UpdateContext = {
                    ...fakeCtx,
                    entities: [entity],
                };

                const state = new Interactive(fakeStory);

                state.update(ctx, input);

                expect(entity.update.mock.calls.length).toBe(1);
                expect(entity.update.mock.calls[0]).toEqual([ctx.screen.tilemap, input]);
            });

            it("should call update on context player if available", () => {
                const player = {
                    ...fakePlayer,
                    update: jest.fn((tilemap, input) => {})
                };
                const ctx: UpdateContext = {
                    ...fakeCtx,
                    player: player,
                };
                const input = {...dummyInput};

                const state = new Interactive(fakeStory);

                state.update(ctx, noInput);

                expect(player.update.mock.calls.length).toBe(1);
                expect(player.update.mock.calls[0]).toEqual([ctx.screen.tilemap, input]);
            });

            it("should choose an interact choice if interact pressed while the player is near another entity", () => {
                const player = {
                    ...fakePlayer,
                    isNear: bb => true,
                };
                const entity: Entity & Interactable = {
                    ...fakeInteractableEntity,
                    interactName: "entity",
                };
                const entityCtx = {
                    ...fakeCtx,
                    player: player,
                    entities: [entity],
                };
                const story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "interact-entity", "three"],
                    choose: jest.fn(i => {}),
                };

                const state = new Interactive(story);

                state
                    .update(entityCtx, noInput)
                    .update(entityCtx, interactInput);

                expect(story.choose.mock.calls.length).toBe(1);
                expect(story.choose.mock.calls[0][0]).toBe("interact-entity");
            });

            it("should not make a choice if interact pressed while the player is not near another entity", () => {
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const entity: Entity & Interactable = {
                    ...fakeInteractableEntity,
                    getBoundingBox: () => new BoundingBox({x:100, y:0}, {x:10, y:10}),
                    interactName: "entity",
                };
                const entityCtx = {
                    ...fakeCtx,
                    player: player,
                    entities: [entity],
                };
                const story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "interact-entity", "three"],
                    choose: jest.fn(i => {}),
                };

                const state = new Interactive(story);

                state
                    .update(entityCtx, noInput)
                    .update(entityCtx, interactInput);

                expect(story.choose.mock.calls.length).toBe(0);
            });

            it("should move to a new state when interacting with an entity", () => {
                const player = {
                    ...fakePlayer,
                    isNear: bb => true,
                };
                const entity: Entity & Interactable = {
                    ...fakeInteractableEntity,
                    interactName: "entity",
                };
                const entityCtx = {
                    ...fakeCtx,
                    player: player,
                    entities: [entity],
                };
                const story: Story = {
                    ...fakeStory,
                    currentChoices: ["one", "two", "interact-entity", "three"],
                    choose: jest.fn(i => {}),
                };

                const state = new Interactive(story);

                const new_state = state
                    .update(entityCtx, noInput)
                    .update(entityCtx, interactInput);

                expect(new_state).not.toBe(state);
            });

            it("should choose direction-up if the player leaves the screen upwards", () => {
                const player = {
                    ...fakePlayer,
                    position: {x: 10, y: -10},
                    getBoundingBox: () => new BoundingBox(
                        {x: 10, y: -10},
                        {x: 10, y: 10}
                    )
                };

                const story = {
                    ...fakeStory,
                    currentChoices: ["foo", "direction-up", "bar"],
                    choose: jest.fn(i => {}),
                };

                const tilemap = {
                    isOffScreen: () => true,
                };

                const ctx = {
                    ...fakeCtx,
                    tilemap: tilemap,
                    player: player,
                }

                const state = new Interactive(story);

                state.update(ctx, noInput);

                expect(story.choose.mock.calls.length).toBe(1);
                expect(story.choose.mock.calls[0][0]).toBe("direction-up");
            });

            it("should choose direction-down if the player leaves the screen upwards", () => {
                const player = {
                    ...fakePlayer,
                    position: {x: 10, y: 1010},
                    getBoundingBox: () => new BoundingBox(
                        {x: 10, y: 1010},
                        {x: 10, y: 10}
                    )
                };

                const story = {
                    ...fakeStory,
                    currentChoices: ["foo", "direction-down", "bar"],
                    choose: jest.fn(i => {}),
                };

                const tilemap = {
                    isOffScreen: () => true,
                };

                const ctx = {
                    ...fakeCtx,
                    tilemap: tilemap,
                    entities: [],
                    player: player,
                }

                const state = new Interactive(story);

                state.update(ctx, noInput);

                expect(story.choose.mock.calls.length).toBe(1);
                expect(story.choose.mock.calls[0][0]).toBe("direction-down");
            });

            /*
            it("should move the player to the right when it leaves the screen to the left", () => {
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox(
                        {x: 1010, y: 10},
                        {x: 10, y: 10}
                    )
                };

                const story: Story = {
                    ...fakeStory,
                    currentChoices: asChoices(["foo", "direction-down", "bar"]),
                    ChooseChoiceIndex: jest.fn(i => {}),
                };

                const tilemap = {
                    isOffScreen: () => true,
                };

                const ctx: UpdateContext = {
                    ...fakeCtx,
                    input: dummyInput,
                    tilemap: tilemap,
                    player: player,
                    entities: {},
                }

                const state = new Interactive(story);

                state.update(ctx);

                expect(story.ChooseChoiceIndex.mock.calls.length).toBe(1);
                expect(story.ChooseChoiceIndex.mock.calls[0][0])
                    .toBe(story.currentChoices.findIndex(c => c.text == "direction-down"));
            });
            */

            it("should call collide on collidable entities that collide with the player", () => {
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };

                const collidable = {
                    ...fakeEntity,
                    collide: jest.fn((player, ctx) => {}),
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };

                const ctx = {
                    ...fakeCtx,
                    entities: [collidable],
                    player: player,
                };

                const state = new Interactive(fakeStory);

                state.update(ctx, noInput);

                expect(collidable.collide.mock.calls.length).toBe(1);
                expect(collidable.collide.mock.calls[0][1]).toBe(player);
            });

            it("should choose the death option if health has reached 0", () => {
                const story = {
                    ...fakeStory,
                    currentChoices: ["foo", "direction-down", "bar", "death"],
                    choose: jest.fn(i => {}),
                };
                const ctx = {
                    ...fakeCtx,
                    health: 0
                };

                const state = new Interactive(story);

                state.update(ctx, noInput);

                expect(story.choose.mock.calls.length).toBe(1);
                expect(story.choose.mock.calls[0][0]).toBe("death");
            })
        });

        describe("updateUI", () => {
            it("should clear the UI content", () => {
                const ui = {
                    ...fakeUI,
                    setContent: jest.fn((content?) => {}),
                };

                const state = new Interactive(fakeStory);

                state.updateUI(fakeCtx, ui);

                expect(ui.setContent.mock.calls.length).toBe(1);
                expect(ui.setContent.mock.calls[0].length).toBe(0);
            });
        });

        describe("getInteractTarget", () => {
            it("should return an entity that is near the player", () => {
                const targetEntity: Entity & Interactable = {
                    ...fakeInteractableEntity,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const ctx: UpdateContext = {
                    ...fakeCtx,
                    entities: [targetEntity],
                    player: player,

                };

                const state = new Interactive(fakeStory);

                expect(state.getInteractTarget(ctx)).toEqual(targetEntity);
            })

            it("should return undefined if no entities are near the player", () => {
                const targetEntity: Entity & Interactable = {
                    ...fakeInteractableEntity,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox({x:100, y:0}, {x:10, y:10}),
                };
                const ctx: UpdateContext = {
                    ...fakeCtx,
                    entities: [targetEntity],
                    player: player,

                };

                const state = new Interactive(fakeStory);

                expect(state.getInteractTarget(ctx)).toBeUndefined();
            })

            it("should not return a non-interactable entity that is near the player", () => {
                const targetEntity: Entity = {
                    ...fakeEntity,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const player = {
                    ...fakePlayer,
                    getBoundingBox: () => new BoundingBox({x:0, y:0}, {x:10, y:10}),
                };
                const ctx: UpdateContext = {
                    ...fakeCtx,
                    entities: [targetEntity],
                    player: player,

                };

                const state = new Interactive(fakeStory);

                expect(state.getInteractTarget(ctx)).toBeUndefined();
            })
        });
    });
});
