import Entity from "./entity";
import InputState from "./inputState";
import Player from "./entities/player";
import Screen from "./screen";
import screensData from "./screens.json";
import Story from "./story";
import UI from "./ui";
import UpdateContext from "./updateContext";
import GameStoryState from "./gameStoryState";

type EntityMap = { [name: string]: Entity };

class GameStory implements UpdateContext {
    story: Story;
    state: GameStoryState;
    player: Player;
    allEntities: EntityMap = {};

    screen: Screen;
    currentEntities: string[] = [];
    _health: number = 0;

    constructor(story: Story, player: Player, state?: GameStoryState) {
        this.story = story;
        this.player = player;

        this.story.observeVariable(
            "screen",
            (_, screenName) => this.updateScreen(screenName)
        );
        this.story.observeVariable(
            "entities",
            (_, entityNames) => this.updateEntities(entityNames)
        );
        this.story.observeVariable(
            "health",
            (_, health) => this.updateHealth(health)
        );

        this.updateState(state);
    }

    update(inputState: InputState) {
        let newState = this.state.update(this, inputState);
        this.state = newState;
    }

    updateUI(ui: UI) {
        ui.setScreenTitle(this.screen.title);
        ui.setHealth(this.health);
        this.state.updateUI(this, ui);
    }

    updateState(state?: GameStoryState) {
        this.state = state ?? GameStoryState.nextState(this.story);
    }

    updateScreen(screenName) {
        this.screen = Screen.fromData(screensData[screenName]);
    }

    updateEntities(entityNamesMap) {
        this.currentEntities = entityNamesMap.orderedItems.map(item => item.Key.itemName);
    }

    updateHealth(health) {
        this._health = health;
    }

    get entities() {
        return this.currentEntities.map(n => this.allEntities[n]).concat(this.screen.entities);
    }

    get health() {
        return this._health;
    }

    set health(value: number) {
        this.story.variablesState["health"] = value;
    }

    addEntity(name: string, entity: Entity) {
        this.allEntities[name] = entity;
    }
}

export default GameStory;
