import debounce, { debounceFunction } from "./debounce";

describe("debounce", () => {
    it("should return false if input is initially true", () => {
        const testDebounce = debounce();

        expect(testDebounce(true)).toEqual(false);
    });

    it("should return true if input is initially true if flage set", () => {
        const testDebounce = debounce(true);

        expect(testDebounce(true)).toEqual(true);
    });

    it("should return true when the input turns true", () => {
        const testDebounce = debounce();

        testDebounce(false);
        expect(testDebounce(true)).toEqual(true);
    });

    it("should return false when input is false", () => {
        const testDebounce = debounce();

        testDebounce(false);
        expect(testDebounce(false)).toEqual(false);
    });

    it("should return true only once when input remains true", () => {
        const testDebounce = debounce();

        testDebounce(false);
        expect(testDebounce(true)).toEqual(true);
        expect(testDebounce(true)).toEqual(false);
        expect(testDebounce(true)).toEqual(false);
        expect(testDebounce(true)).toEqual(false);
    });

    it("should return true again after input becomes false", () => {
        const testDebounce = debounce();

        testDebounce(false);
        expect(testDebounce(true)).toEqual(true);
        expect(testDebounce(true)).toEqual(false);
        expect(testDebounce(true)).toEqual(false);
        expect(testDebounce(true)).toEqual(false);

        // input "released"
        expect(testDebounce(false)).toEqual(false);

        expect(testDebounce(true)).toEqual(true);
        expect(testDebounce(true)).toEqual(false);
        expect(testDebounce(true)).toEqual(false);
    });
});
