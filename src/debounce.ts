type debounceFunction = (input: boolean) => boolean;

function debounce(allowInitial: boolean = false): debounceFunction {
    let debounced = allowInitial;

    return (input) => {
        const result = debounced && input;
        debounced = !input;

        return result;
    }
}

export default debounce;
export { debounceFunction };
