interface UI {
    setScreenTitle: (string) => void;
    setContent: (string?) => void;
    setChoices: (choices: string[], number) => void;
    setInteractHint: (string?) => void;
    setHealth: (number) => void;
}

class HtmlUI implements UI {
    setScreenTitle(title: string) {
        const titleElement: HTMLElement = document.getElementById('screen-name');
        titleElement.innerText = title;
    }

    setContent(content?: string) {
        const contentElement: HTMLElement = document.getElementById('story-content');
        contentElement.classList.remove('shown');
        if (content) {
            contentElement.innerText = content;
            contentElement.classList.add('shown');
        }
    }

    setChoices(choices: string[], selected: number) {
        const contentElement: HTMLElement = document.getElementById('story-content');
        contentElement.classList.remove('shown');
        contentElement.innerText = "";
        const choicesList = document.createElement("ol");
        for (let idx in choices) {
            let choiceItem = document.createElement("li");
            choiceItem.innerText = choices[idx];
            choiceItem.classList.toggle("selected", parseInt(idx, 10) === selected);
            choicesList.appendChild(choiceItem);
        }
        contentElement.appendChild(choicesList);
        contentElement.classList.add('shown');
    }

    setInteractHint(content?: string) {
        const contentElement: HTMLElement = document.getElementById('interact-hint');
        contentElement.classList.remove('shown');
        if (content) {
            contentElement.innerText = content;
            contentElement.classList.add('shown');
        }
    }

    setHealth(health: number) {
        const contentElement: HTMLElement = document.getElementById('health');
        contentElement.innerText = `${health}`;
    }
}

export default UI
export {
    HtmlUI,
}
