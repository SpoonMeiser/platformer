import initEntities from "./entities/init"
import Entity from "./entity";
import GameStory from "./gameStory";
import InputState from "./inputState";
import Player from "./entities/player";
import { StoryWrapper } from "./story";
import story_data from "./story.json";
import Tilemap from "./tilemap";
import UI, { HtmlUI } from "./ui";

class World {
    player: Player;
    ctx: CanvasRenderingContext2D;
    gameStory: GameStory;
    input_state: InputState;
    ui: UI;

    constructor(ctx: CanvasRenderingContext2D, player: Player, input_state: InputState) {
        this.player = player;
        this.ctx = ctx;
        this.ui = new HtmlUI();
        const story = new StoryWrapper(story_data);
        this.gameStory = new GameStory(story, this.player);
        this.input_state = input_state;

        initEntities(this.gameStory)
    }

    render(tilemap: Tilemap, entities: Entity[]) {
        this.ctx.clearRect(0, 0, 1000, 500);
        tilemap.debugDraw(this.ctx);
        entities.forEach(c => c.draw(this.ctx));
        this.player.draw(this.ctx);
    }

    step() {
        this.gameStory.update(this.input_state);
        this.gameStory.updateUI(this.ui);

        this.render(this.gameStory.screen.tilemap, this.gameStory.entities);
    }

    loop() {
        try {
            this.step();
        }
        catch(e) {
            console.error(e);
        }

        window.requestAnimationFrame(this.loop.bind(this));
    }
}

export default World;
