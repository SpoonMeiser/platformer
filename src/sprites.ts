import { Vector } from "./boundingBox"

interface Sprite {
    img: HTMLImageElement,
    offset: Vector,
}

type SpriteMap = { [name: string]: Sprite }

const spriteMap: SpriteMap = {}

export default spriteMap
export {
    Sprite
}
