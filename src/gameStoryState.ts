import debounce, { debounceFunction } from "./debounce";
import Story from "./story";
import UpdateContext from "./updateContext";
import InputState from "./inputState";
import Entity, {Interactable, isAlsoInteractable, isAlsoCollidable} from "./entity";
import UI from "./ui";

abstract class GameStoryState {
    story: Story;

    constructor(story: Story) {
        this.story = story;
    }

    static nextState(story: Story): GameStoryState {
        const content = story.next();
        if (content) {
            return new Content(story, content);
        }

        if (story.variablesState["interactive"]) {
            return new Interactive(story);
        }
        else {
            return new Choices(story);
        }
    }

    abstract update(ctx: UpdateContext, input: InputState): GameStoryState;

    abstract updateUI(ctx: UpdateContext, ui: UI): void;
}

class Content extends GameStoryState {
    content: string;
    debounceInteract: debounceFunction = debounce();

    constructor(story, content?) {
        super(story);
        const actualContent = content ?? story.next();
        this.content = actualContent;
    }

    update(ctx, input) {
        if (this.debounceInteract(input.interact)) {
            return GameStoryState.nextState(this.story);
        }

        return this;
    }

    updateUI(ctx, ui) {
        ui.setContent(this.content);
    }
}

class Interactive extends GameStoryState {
    debounceInteract: debounceFunction = debounce();

    getInteractTarget(ctx: UpdateContext): Interactable | undefined {
        const playerBoundingBox = ctx.player.getBoundingBox();
        const isNearPlayer = (entity: Entity): boolean =>
            playerBoundingBox.isNear(entity.getBoundingBox());

        return ctx.entities.filter(isAlsoInteractable).find(isNearPlayer);
    }

    updatePlayer(ctx, input) {
        ctx.player.update(ctx.screen.tilemap, input);

        if (ctx.screen.tilemap.isOffScreen(ctx.player.getBoundingBox())) {
            const {x, y} = ctx.player.getBoundingBox().getCenter();
            let choice: string;
            if (x > 1000) {
                choice = 'direction-right';
                ctx.player.position.x -= 1000;
            }
            if (x < 0) {
                choice = 'direction-left';
                ctx.player.position.x += 1000;
            }
            if (y > 500) {
                choice = 'direction-down';
                ctx.player.position.y -= 500;
            }
            if (y < 0) {
                choice = 'direction-up';
                ctx.player.position.y += 500;
            }
            this.story.choose(choice);
            return GameStoryState.nextState(this.story);
        }

        if (this.debounceInteract(input.interact)) {
            const target = this.getInteractTarget(ctx);
            if (target) {
                const choice = `interact-${target.interactName}`;

                this.story.choose(choice);
                return GameStoryState.nextState(this.story);
            }
        }

        return null;
    }

    update(ctx, input) {
        let nextState: GameStoryState = this;

        if (ctx.health <= 0) {
            this.story.choose("death");
            nextState = GameStoryState.nextState(this.story);
        }
        else if (ctx.player) {
            nextState = this.updatePlayer(ctx, input) ?? nextState;
        }

        ctx.entities.forEach((e: Entity) => {
            e.update(ctx.screen.tilemap, input);

            if (isAlsoCollidable(e)) {
                const playerBoundingBox = ctx.player.getBoundingBox();
                const entityBoundingBox = e.getBoundingBox();

                if (entityBoundingBox.collides(playerBoundingBox)) {
                    e.collide(ctx, ctx.player);
                }
            }
        });

        return nextState;
    }

    updateUI(ctx, ui) {
        const target = this.getInteractTarget(ctx);
        if (target) {
            ui.setInteractHint(target.getInteractHint());
        }
        else {
            ui.setInteractHint();
        }
        ui.setContent();
    }
}

class Choices extends GameStoryState {
    choices: string[];
    choice: number;
    debounceInteract: debounceFunction = debounce();
    debounceUp: debounceFunction = debounce();
    debounceDown: debounceFunction = debounce();

    constructor(story, choice = 0) {
        super(story);
        const choices = story.currentChoices;
        this.choices = choices;

        // JS modulo doesn't handle negative numbers in the way you might expect
        const n = this.choices.length;
        this.choice = ((choice % n) + n) % n;
    }

    update(ctx, input) {
        if (this.debounceInteract(input.interact)) {
            this.story.choose(this.choice);
            return GameStoryState.nextState(this.story);
        }

        if (this.debounceDown(input.down)) {
            return new Choices(this.story, this.choice + 1);
        }

        if (this.debounceUp(input.up)) {
            return new Choices(this.story, this.choice - 1);
        }

        return this;
    }

    updateUI(ctx, ui) {
        ui.setChoices(this.choices, this.choice);
    }
}

export default GameStoryState
export {
    Content,
    Interactive,
    Choices,
};
