import BoundingBox, { Vector } from "./boundingBox";
import levels from "./test_levels.json";
import Tilemap, { split_vector } from "./tilemap";

describe("Tilemap", () => {
    const empty_map = levels.tilemaps.empty;
    const full_map = levels.tilemaps.full;
    const x_at_20_10_map = levels.tilemaps.x_at_20_10;

    // 20,10 tile coordinates maps to 400,200 - 420,220

    it("should be constructed somehow", () => {
        const tilemap = new Tilemap(empty_map);
    });

    /*
    it("should raise an error for wrong number of rows", () => {
        const too_few_rows_map = empty_map.slice(0, 10);
        expect(() => new Tilemap(too_few_rows_map)).toThrow(/wrong number of rows/i);
    });

    it("should raise an error for wrong number of columns", () => {
        const too_few_columns_map = empty_map.map(() => "   ");
        expect(() => new Tilemap(too_few_columns_map)).toThrow(/wrong number of columns/i);
    });
    */

    it("should not alter data passed to constructor", () => {
        const data = [" "];
        const tilemap = new Tilemap(data);

        tilemap.addTile("X", 0, 0)

        // data should be unchanged when a tilemap created from it is modified
        expect(data).toEqual([" "]);
    });

    describe("collides", () => {
        const random_boxes = [1, 2, 3, 4].map(() => {
            return new BoundingBox(
                {
                    x: Math.random() * 900,
                    y: Math.random() * 400
                },
                {
                    x: Math.random() * 200,
                    y: Math.random() * 200
                }
            );
        });

        it("should return false for any box in an empty map", () => {
            const tilemap = new Tilemap(empty_map);
            const some_boxes_collide = random_boxes.some(b => tilemap.collides(b));

            expect(some_boxes_collide).toBe(false);
        });

        it("should return true for any box in a full map", () => {
            const tilemap = new Tilemap(full_map);
            const all_boxes_collide = random_boxes.every(b => tilemap.collides(b));

            expect(all_boxes_collide).toBe(true);
        });

        it("should return true for a box fully inside a tile", () => {
            const tilemap = new Tilemap(x_at_20_10_map);
            const box = new BoundingBox(
                {x: 401, y: 201},
                {x: 18, y: 18}
            );

            expect(tilemap.collides(box)).toBe(true);
        });

        it("should return true for a box that overlaps a tile edge", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            const box_left = new BoundingBox({x: 395, y: 201}, {x: 18, y: 18});
            const box_right = new BoundingBox({x: 405, y: 201}, {x: 18, y: 18});
            const box_top = new BoundingBox({x: 401, y: 195}, {x: 18, y: 18});
            const box_bottom = new BoundingBox({x: 401, y: 205}, {x: 18, y: 18});

            expect(tilemap.collides(box_left)).toBe(true);
            expect(tilemap.collides(box_right)).toBe(true);
            expect(tilemap.collides(box_top)).toBe(true);
            expect(tilemap.collides(box_bottom)).toBe(true);
        });

        it("should return appropriately for partially out of bounds box", () => {
            const box_left = new BoundingBox({x: -50, y: 200}, {x: 100, y: 100});
            const box_right = new BoundingBox({x: 950, y: 200}, {x: 100, y: 100});
            const box_top = new BoundingBox({x: 400, y: -50}, {x: 100, y: 100});
            const box_bottom = new BoundingBox({x: 400, y: 450}, {x: 100, y: 100});

            const full_tilemap = new Tilemap(full_map);
            expect(full_tilemap.collides(box_left)).toBe(true);
            expect(full_tilemap.collides(box_right)).toBe(true);
            expect(full_tilemap.collides(box_top)).toBe(true);
            expect(full_tilemap.collides(box_bottom)).toBe(true);

            const empty_tilemap = new Tilemap(empty_map);
            expect(empty_tilemap.collides(box_left)).toBe(false);
            expect(empty_tilemap.collides(box_right)).toBe(false);
            expect(empty_tilemap.collides(box_top)).toBe(false);
            expect(empty_tilemap.collides(box_bottom)).toBe(false);
        });

        it("should return false for box fully out of bounds", () => {
            const box = new BoundingBox({x: 2000, y:0}, {x: 50, y: 50});
            const tilemap = new Tilemap(full_map);

            expect(tilemap.collides(box)).toBe(false);
        });

        describe("tile '-'", () => {
            const step_up: Vector = {x: 0, y: -1};
            const step_down: Vector = {x: 0, y: 1};
            const step_left: Vector = {x: -1, y: 0};
            const step_right: Vector = {x: 1, y: 0};

            it("should return false if moving up", () => {
                const tilemap = new Tilemap(empty_map);
                tilemap.addTile('-', 20, 10);

                const box = new BoundingBox({x: 405, y: 219}, {x: 10, y: 10});

                expect(tilemap.collides(box, step_up)).toBe(false);
            });

            it("should return true if moving down from above the tile", () => {
                const tilemap = new Tilemap(empty_map);
                tilemap.addTile('-', 20, 10);

                const box = new BoundingBox({x: 405, y: 191}, {x: 10, y: 10});

                expect(tilemap.collides(box, step_down)).toBe(true);
            });

            it("should return false if moving down from inside the tile", () => {
                const tilemap = new Tilemap(empty_map);
                tilemap.addTile('-', 20, 10);

                const box = new BoundingBox({x: 405, y: 192}, {x: 10, y: 10});

                expect(tilemap.collides(box, step_down)).toBe(false);
            });

            it("should return false if moving left or right", () => {
                const tilemap = new Tilemap(empty_map);
                tilemap.addTile('-', 20, 10);

                const box = new BoundingBox({x: 405, y: 205}, {x: 10, y: 10});

                expect(tilemap.collides(box, step_left)).toBe(false);
                expect(tilemap.collides(box, step_right)).toBe(false);
            });
        });
    });

    describe("supports", () => {
        it("should return false for a box in an empty map", () => {
            const box = new BoundingBox({x: 500, y:50}, {x: 50, y: 50});
            const tilemap = new Tilemap(empty_map);

            expect(tilemap.supports(box)).toBe(false);
        });

        it("should return true for a box on a tile", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            expect(tilemap.supports(new BoundingBox({x: 400, y: 150}, {x: 50, y: 50}))).toBe(true);
            expect(tilemap.supports(new BoundingBox({x: 410, y: 150}, {x: 50, y: 50}))).toBe(true);
            expect(tilemap.supports(new BoundingBox({x: 360, y: 150}, {x: 50, y: 50}))).toBe(true);
        });

        it("should return false if the box extends below the tile is supposed to support it", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            expect(tilemap.supports(new BoundingBox({x: 400, y: 190}, {x: 50, y: 50}))).toBe(false);
            expect(tilemap.supports(new BoundingBox({x: 410, y: 190}, {x: 50, y: 50}))).toBe(false);
            expect(tilemap.supports(new BoundingBox({x: 360, y: 190}, {x: 50, y: 50}))).toBe(false);
        });

        it("should return true for a box on a '-' tile", () => {
            const tilemap = new Tilemap(empty_map);
            tilemap.addTile('-', 20, 10);

            expect(tilemap.supports(new BoundingBox({x: 400, y: 150}, {x: 50, y: 50}))).toBe(true);
            expect(tilemap.supports(new BoundingBox({x: 410, y: 150}, {x: 50, y: 50}))).toBe(true);
            expect(tilemap.supports(new BoundingBox({x: 360, y: 150}, {x: 50, y: 50}))).toBe(true);
        });

        it("should return false for a box in but not on a '-' tile", () => {
            const tilemap = new Tilemap(empty_map);
            tilemap.addTile('-', 20, 10);

            expect(tilemap.supports(new BoundingBox({x: 400, y: 160}, {x: 50, y: 50}))).toBe(false);
            expect(tilemap.supports(new BoundingBox({x: 410, y: 180}, {x: 50, y: 50}))).toBe(false);
            expect(tilemap.supports(new BoundingBox({x: 360, y: 210}, {x: 50, y: 50}))).toBe(false);
        });
    });

    describe("resolve_movement", () => {
        it("should return unaltered velocity and position added to velocity when not colliding", () => {
            const tilemap = new Tilemap(empty_map);

            const bounding_box = new BoundingBox({x: 0, y: 0}, {x: 50, y: 50});
            const velocity = {x: 10, y: 10};

            const { new_position, new_velocity } = tilemap.resolve_movement(bounding_box, velocity);

            expect(new_position.x).toEqual(10);
            expect(new_position.y).toEqual(10);
            expect(new_velocity.x).toEqual(10);
            expect(new_velocity.y).toEqual(10);
        });

        it("should stop when movement is directly down into a tile", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            const box = new BoundingBox({x: 385, y: 145}, {x: 50, y: 50});
            const velocity = {x: 0, y: 10};

            const { new_position, new_velocity } = tilemap.resolve_movement(box, velocity);

            expect(new_position.x).toEqual(385);
            expect(new_position.y).toEqual(150);
            expect(new_velocity.x).toEqual(0);
            expect(new_velocity.y).toEqual(0);
        });

        it("should stop when movement is directly up into a tile", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            const box = new BoundingBox({x: 385, y: 225}, {x: 50, y: 50});
            const velocity = {x: 0, y: -10};

            const { new_position, new_velocity } = tilemap.resolve_movement(box, velocity);

            expect(new_position.x).toEqual(385);
            expect(new_position.y).toEqual(220);
            expect(new_velocity.x).toEqual(0);
            expect(new_velocity.y).toEqual(0);
        });

        it("should stop when movement is directly right into a tile", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            const box = new BoundingBox({x: 345, y: 185}, {x: 50, y: 50});
            const velocity = {x: 10, y: 0};

            const { new_position, new_velocity } = tilemap.resolve_movement(box, velocity);

            expect(new_position.x).toEqual(350);
            expect(new_position.y).toEqual(185);
            expect(new_velocity.x).toEqual(0);
            expect(new_velocity.y).toEqual(0);
        });

        it("should slide along a tile when hit at an angle", () => {
            const tilemap = new Tilemap(x_at_20_10_map);

            const box = new BoundingBox({x: 385, y: 145}, {x: 50, y: 50});
            const velocity = {x: 5, y: 10};

            const { new_position, new_velocity } = tilemap.resolve_movement(box, velocity);

            expect(new_position.x).toEqual(390);
            expect(new_position.y).toEqual(150);
            expect(new_velocity.x).toEqual(5);
            expect(new_velocity.y).toEqual(0);
        });

        it("should stop at the point the box is off screen", () => {
            const tilemap = new Tilemap(empty_map);

            const box = new BoundingBox({x: 385, y: 5}, {x: 50, y: 50});
            const velocity = {x: 0, y: -100};

            const { new_position, new_velocity } = tilemap.resolve_movement(box, velocity);

            expect(new_position.x).toEqual(385);
            expect(new_position.y).toEqual(-26);
            expect(new_velocity.x).toEqual(0);
            expect(new_velocity.y).toEqual(-100);
        });
    })

    describe("isOffScreen", () => {
        it("should return false for a box fully in the screen", () => {
            const tilemap = new Tilemap(empty_map);

            const box = new BoundingBox({x: 385, y: 145}, {x: 50, y: 50});

            expect(tilemap.isOffScreen(box)).toBe(false);
        })

        it("should return true for a box fully off the screen", () => {
            const tilemap = new Tilemap(empty_map);

            const boxes = [
                new BoundingBox({x: -200, y: 145}, {x: 50, y: 50}),
                new BoundingBox({x: 385, y: -200}, {x: 50, y: 50}),
                new BoundingBox({x: 2000, y: 145}, {x: 50, y: 50}),
                new BoundingBox({x: 385, y: 2000}, {x: 50, y: 50}),
            ];

            const all_isOffScreen = boxes.every(box => tilemap.isOffScreen(box))
            expect(all_isOffScreen).toBe(true);
        })

        it("should return false for a box partially off screen", () => {
            const tilemap = new Tilemap(empty_map);

            const boxes = [
                new BoundingBox({x: -10, y: 145}, {x: 50, y: 50}),
                new BoundingBox({x: 385, y: -10}, {x: 50, y: 50}),
                new BoundingBox({x: 960, y: 145}, {x: 50, y: 50}),
                new BoundingBox({x: 385, y: 460}, {x: 50, y: 50}),
            ];

            const any_isOffScreen = boxes.some(box => tilemap.isOffScreen(box))
            expect(any_isOffScreen).toBe(false);
        })

        it("should return true for a box half off screen", () => {
            const tilemap = new Tilemap(empty_map);

            const boxes = [
                new BoundingBox({x: -6, y: 145}, {x: 10, y: 10}),
                new BoundingBox({x: 385, y: -6}, {x: 10, y: 10}),
                new BoundingBox({x: 996, y: 145}, {x: 10, y: 10}),
                new BoundingBox({x: 385, y: 496}, {x: 10, y: 10}),
            ];

            const all_isOffScreen = boxes.every(box => tilemap.isOffScreen(box))
            expect(all_isOffScreen).toBe(true);
        })
    });
});

describe("split_vector", () => {
    const vector_sum_reducer = (acc, cur) => {
        return {x: acc.x + cur.x, y: acc.y + cur.y};
    };
    const sum_vectors = vs => vs.reduce(vector_sum_reducer, {x: 0, y: 0});

    it("should return as many vectors as the manhatten distance", () => {
        expect(split_vector({x: 10, y: 0})).toHaveLength(10);
        expect(split_vector({x: 3, y: 7})).toHaveLength(10);
        expect(split_vector({x: 2, y: -5})).toHaveLength(7);
        expect(split_vector({x: -10, y: -6})).toHaveLength(16);
        expect(split_vector({x: 0, y: 0})).toHaveLength(0);
    });

    it("should return an array of vectors", () => {
        split_vector({x: 10, y: 10}).forEach(v => {
            expect(v).toHaveProperty('x');
            expect(v).toHaveProperty('y');
        })
    });

    it("should return vectors that add up to the original vector", () => {
        const test_vectors = [
            {x: 0, y: 0},
            {x: 10, y: 0},
            {x: -2, y: 7},
            {x: -5, y: -5},
            {x: -10, y: -6},
        ];

        test_vectors.forEach(v => {
            expect(sum_vectors(split_vector(v))).toEqual(v);
        });
    });

    it("should spread steps in the minor direction vaguely evenly", () => {
        const vectors = split_vector({x: 2, y: 100});
        const v1 = vectors.slice(0, 51);
        const v2 = vectors.slice(51, 102);

        expect(sum_vectors(v1)).toEqual({x: 1, y: 50});
        expect(sum_vectors(v2)).toEqual({x: 1, y: 50});
    });
});
