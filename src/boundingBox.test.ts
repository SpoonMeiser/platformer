import BoundingBox from "./boundingBox";

describe("BoundingBox", () => {
    describe("collide", () => {
        it("should return false for a box where position x and y both beyond position + dimension", () => {
            const b1 = new BoundingBox({x: 10, y: 10}, {x: 5, y: 5});
            const b2 = new BoundingBox({x: 20, y: 20}, {x: 5, y: 5});

            expect(b1.collides(b2)).toBe(false);
        });

        it("should return true for a box inside this", () => {
            const b1 = new BoundingBox({x: 10, y: 10}, {x: 9, y: 9});
            const b2 = new BoundingBox({x: 12, y: 12}, {x: 5, y: 5});

            expect(b1.collides(b2)).toBe(true);
        });

        it("should return false for a box inside one dimension but outside the other", () => {
            const b1 = new BoundingBox({x: 50, y: 50}, {x: 9, y: 9});
            const other_boxes = [
                new BoundingBox({x: 99, y: 52}, {x: 5, y: 5}),
                new BoundingBox({x: 52, y: 99}, {x: 5, y: 5}),
                new BoundingBox({x: 0, y: 52}, {x: 5, y: 5}),
                new BoundingBox({x: 52, y: 0}, {x: 5, y: 5}),
            ]

            other_boxes.forEach(b => {
                expect(b1.collides(b)).toBe(false);
            });
        });

        it("should return true for a box that overlaps", () => {
            const b1 = new BoundingBox({x: 40, y: 40}, {x: 20, y: 20});
            const other_boxes = [
                new BoundingBox({x: 35, y: 35}, {x: 10, y: 10}),
                new BoundingBox({x: 45, y: 35}, {x: 10, y: 10}),
                new BoundingBox({x: 55, y: 35}, {x: 10, y: 10}),

                new BoundingBox({x: 35, y: 45}, {x: 10, y: 10}),
                new BoundingBox({x: 55, y: 45}, {x: 10, y: 10}),

                new BoundingBox({x: 35, y: 55}, {x: 10, y: 10}),
                new BoundingBox({x: 45, y: 55}, {x: 10, y: 10}),
                new BoundingBox({x: 55, y: 55}, {x: 10, y: 10}),
            ]

            other_boxes.forEach(b => {
                expect(b1.collides(b)).toBe(true);
            });
        });

        it("should return false for boxes that only touch", () => {
            const b1 = new BoundingBox({x: 40, y: 40}, {x: 20, y: 20});
            const b2 = new BoundingBox({x: 40, y: 60}, {x: 20, y: 20});

            expect(b1.collides(b2)).toBe(false);
        });
    });

    it("should return its centre position", () => {
        const b = new BoundingBox({x: 10, y:100}, {x: 20, y: 10});

        expect(b.getCenter()).toEqual({x: 20, y: 105});
    });

    it("should be be embiggenable", () => {
        const embiggen_by = 10;
        const b = new BoundingBox({x: 10, y:100}, {x: 20, y: 10});

        const c = b.embiggen(embiggen_by);

        expect(b.position.x - c.position.x).toEqual(embiggen_by);
        expect(b.position.y - c.position.y).toEqual(embiggen_by);
        expect(c.dimension.x - b.dimension.x).toEqual(embiggen_by * 2);
        expect(c.dimension.y - b.dimension.y).toEqual(embiggen_by * 2);
    });

    describe("isNear", () => {
        it("should return false for far away bounding box", () => {
            const box = new BoundingBox(
                {x: 0, y: 0},
                {x: 50, y: 50}
            );

            const other = new BoundingBox(
                {x: 500, y: 500},
                {x: 10, y: 10}
            );

            expect(box.isNear(other)).toBe(false);
        });

        it("should return true for near bounding box", () => {
            const box = new BoundingBox(
                {x: 100, y: 100},
                {x: 50, y: 50}
            );

            const other = new BoundingBox(
                {x: 100, y: 80},
                {x: 10, y: 10}
            );

            expect(box.isNear(other)).toBe(true);
        });
    })
});
