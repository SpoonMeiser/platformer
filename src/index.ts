import { CombinedInput } from "./inputState";
import Player from "./entities/player";
import World from "./world";
import initSprites from "./initSprites"

function get_ctx() {
    const canvas = <HTMLCanvasElement> document.getElementById('game');
    const ctx = canvas.getContext('2d');
    return ctx;
}

function resize() {
    const container = document.getElementById('game-container');
    const canvas = <HTMLCanvasElement> document.getElementById('game');

    const scale = container.clientWidth / 1000;

    canvas.width = 1000 * scale;
    canvas.height = 500 * scale;

    const ctx = canvas.getContext('2d');
    ctx.scale(scale, scale);
}

async function main() {
    resize();
    window.addEventListener("resize", resize)

    console.log("initialising sprites")
    await initSprites()
    console.log("sprites loaded")

    const ctx = get_ctx();
    const player = new Player();
    const input_state = new CombinedInput(window);
    const world = new World(ctx, player, input_state);

    world.loop();
}

main();
