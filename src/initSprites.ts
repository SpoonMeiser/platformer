import spriteMap from "./sprites";

const sarah_standing_right_1_url = new URL("../assets/sarah-standing-right-1.svg", import.meta.url)
const sarah_run_right_1_url = new URL("../assets/sarah-run-right-1.svg", import.meta.url)
const sarah_run_right_2_url = new URL("../assets/sarah-run-right-2.svg", import.meta.url)
const sarah_run_right_3_url = new URL("../assets/sarah-run-right-3.svg", import.meta.url)
const sarah_run_right_4_url = new URL("../assets/sarah-run-right-4.svg", import.meta.url)
const sarah_run_right_5_url = new URL("../assets/sarah-run-right-5.svg", import.meta.url)
const sarah_run_right_6_url = new URL("../assets/sarah-run-right-6.svg", import.meta.url)
const sarah_standing_left_1_url = new URL("../assets/sarah-standing-left-1.svg", import.meta.url)
const sarah_run_left_1_url = new URL("../assets/sarah-run-left-1.svg", import.meta.url)
const sarah_run_left_2_url = new URL("../assets/sarah-run-left-2.svg", import.meta.url)
const sarah_run_left_3_url = new URL("../assets/sarah-run-left-3.svg", import.meta.url)
const sarah_run_left_4_url = new URL("../assets/sarah-run-left-4.svg", import.meta.url)
const sarah_run_left_5_url = new URL("../assets/sarah-run-left-5.svg", import.meta.url)
const sarah_run_left_6_url = new URL("../assets/sarah-run-left-6.svg", import.meta.url)

const loadImage = (url: URL): Promise<HTMLImageElement> => {
    return new Promise((resolve, reject) => {
        let img = new Image()
        img.onload = () => resolve(img)
        img.onerror = e => reject(e)
        img.src = url.toString()
    })
}

const loadSprite = (name: string, url: URL, xOff: number, yOff: number): Promise<void> => {
    return loadImage(url).then(img => {
        spriteMap[name] = {img: img, offset: {x: xOff, y: yOff}}
    })
}

const initSprites = (): Promise<void[]> => {
    return Promise.all([
        loadSprite("sarah-standing-right-1", sarah_standing_right_1_url, -5, -3),
        loadSprite("sarah-run-right-1", sarah_run_right_1_url, -5, -3),
        loadSprite("sarah-run-right-2", sarah_run_right_2_url, -5, -3),
        loadSprite("sarah-run-right-3", sarah_run_right_3_url, -5, -3),
        loadSprite("sarah-run-right-4", sarah_run_right_4_url, -5, -3),
        loadSprite("sarah-run-right-5", sarah_run_right_5_url, -5, -3),
        loadSprite("sarah-run-right-6", sarah_run_right_6_url, -5, -3),
        loadSprite("sarah-standing-left-1", sarah_standing_left_1_url, -1, -3),
        loadSprite("sarah-run-left-1", sarah_run_left_1_url, -5, -3),
        loadSprite("sarah-run-left-2", sarah_run_left_2_url, -1, -3),
        loadSprite("sarah-run-left-3", sarah_run_left_3_url, -2, -3),
        loadSprite("sarah-run-left-4", sarah_run_left_4_url, -4, -3),
        loadSprite("sarah-run-left-5", sarah_run_left_5_url, -3, -3),
        loadSprite("sarah-run-left-6", sarah_run_left_6_url, -1, -3),
    ])
}

export default initSprites
