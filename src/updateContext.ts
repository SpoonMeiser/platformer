import Entity from "./entity";
import Screen from "./screen";


interface UpdateContext {
    entities: Entity[],
    player: Entity | null,
    screen: Screen,
    health: number,
}


export default UpdateContext;
